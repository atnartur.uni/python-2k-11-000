def my_iter(start=0):
	current = start
	while True:
		yield current
		current += 1
		print('iter')


count = 0 
for i in my_iter():
	print(i)
	count += 1
	if count == 10:
		break


class MyIter:
    def __iter__(self):
        return self

    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return self.counter
        else:
            raise StopIteration


iter = MyIter(10)
print(iter)
for i in iter:
	print(i)


def func(x):
	return x * 2


iter2 = MyIter(10)
map_iter = map(lambda x: x * 2, iter2)
filter_iter = filter(lambda x: x != 2, map_iter)
print(filter_iter)
print(list(filter_iter))


iter3 = MyIter(10)
mapped = [x * 2 for x in iter3]
filtered = [x for x in mapped if x != 2]
print(filtered)



iter4 = MyIter(10)
mapped2 = (x * 2 for x in iter4)
filtered2 = (x for x in mapped2 if x != 2)
print(filtered2)
print(list(filtered2))