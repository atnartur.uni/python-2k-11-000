from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from web.models import User


def main(request: HttpRequest):
    """
    Главная страница
    :param request: запрос
    :return: шаблон главной страницы
    """
    user = User.objects.first()
    print(user.email_upper)
    context = {
        'user_email_upper': User.objects.first().email_upper
    }

    return render(request, 'web/main.html', context)
