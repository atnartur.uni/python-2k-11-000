from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager as DjangoUserManager, PermissionsMixin


class UserManager(DjangoUserManager):
    def create_user(self, username: str, password: str=None, **extra_fields):
        """
        Создает пользователя
        :param username: имя пользователя (email)
        :param password: пароль
        :param extra_fields: другие параметры пользователя
        :return:
        """
        user = self.model(email=username)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email=None, password=None, **extra_fields):
        user = self.model(email=email, is_superuser=True, is_staff=True)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()

    USERNAME_FIELD = 'email'
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=128)
    is_staff = models.BooleanField(default=False)

    @property
    def email_upper(self):
        return self.email.upper()
