import random
import traceback


try:
    number = int(input())
    number2 = int(input())
    print(number / number2)
except (ZeroDivisionError, ValueError) as exc:
    print('введены неправильные данные')
    print(type(exc))
    print(traceback.format_exc())
except BaseException:
    print('произошло что-то невероятное')


class MyException(BaseException):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message

def my_func():
    if random.random() > 0.5:
        raise MyException("exception from my func")

for i in range(10):
    print(i)
    my_func()
