from behave import *


class Cat():
    def __init__(self, name):
        self.name = name
        self.fullness = 0

    def eat(self):
        self.fullness += 10

    def __str__(self):
        return self.name


@given("cat")
def create_cat(context):
    context.cat = Cat("Барсик")


@when('eat')
def eating(context):
    context.cat.eat()


@then('fullness has increased to {number}')
def check_fullness(context, number):
    assert context.cat.fullness == int(number)
