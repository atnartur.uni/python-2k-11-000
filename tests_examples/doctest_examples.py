def div(x, y):
    """
    Деление

    >>> div(4, 2)
    2.0

    >>> div(6, 3)
    2.0

    :param x: первый аргумент
    :param y: второй аргумент
    :return: результат деления
    """
    return x / y


if __name__ == '__main__':
    import doctest

    doctest.testmod()
