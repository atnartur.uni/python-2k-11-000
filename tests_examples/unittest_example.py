import random
import unittest


class Toy():
    def __init__(self, name, is_for_cat):
        self.name = name
        self.is_for_cat = is_for_cat

    def __str__(self):
        return self.name


TOYS = [
    Toy('piece_of_paper', True),
    Toy('toy_mouse', True),
    Toy('pc_mouse', False),
    Toy('phone', False),
]


class Cat():
    def __init__(self, name):
        self.name = name
        self.fullness = 0

    def eat(self):
        self.fullness += 10

    def select_a_toy(self):
        cat_toys = list(filter(lambda x: x.is_for_cat, TOYS))
        return random.choice(cat_toys)

    def __str__(self):
        return self.name


class TestCat(unittest.TestCase):
    def test_eat(self):
        cat = Cat('Барсик')
        cat.eat()
        self.assertEqual(10, cat.fullness)

    def test_toy_selection(self):
        cat = Cat('Барсик')
        selected_toy = cat.select_a_toy()
        self.assertIsInstance(selected_toy, Toy)


if __name__ == "__main__":
    unittest.main()
