import os

currdir = os.getcwd()

new_dir = os.path.join(currdir, '..')  # f'{currdir}/..'
os.chdir(new_dir)
print(os.getcwd())

file_path = os.path.join(new_dir, 'test.txt')
file = open(file_path, 'w')
file.write('hello world')
file.close()

file = open(os.path.join(currdir, 'main.py'), 'r')
print(file.read())
file.close()


