import multiprocessing
import random


def generate_numbers(thread_name, array):
	for i in range(10):
		number = random.randrange(0, 100)
		print(thread_name, i, number)
		array[i] = number


if __name__ == '__main__':
	array = multiprocessing.Array('i', 10)

	process1 = multiprocessing.Process(
		target=generate_numbers, 
		args=('first', array)
	)
	process2 = multiprocessing.Process(
		target=generate_numbers, 
		args=('second', array)
	)

	process1.start()
	process2.start()
	process1.join()
	process2.join()

	for item in array:
		print(item)
