import threading
import random


def generate_numbers(thread_name, event1, event2):
	for i in range(10):
		event1.wait()
		print(thread_name, i, random.randrange(0, 100))
		event1.clear()
		event2.set()


event1 = threading.Event()
event2 = threading.Event()

thread1 = threading.Thread(
	target=generate_numbers,
	args=('first', event1, event2)
)
thread2 = threading.Thread(
	target=generate_numbers,
	args=('second', event2, event1)
)
thread1.start()
thread2.start()
event1.set()