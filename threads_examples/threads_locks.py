import threading
import random


def generate_numbers(thread_name, lock):
	for i in range(10):
		lock.acquire()
		print(thread_name, i, random.randrange(0, 100))
		lock.release()


lock = threading.Lock()

thread1 = threading.Thread(
	target=generate_numbers,
	args=('first', lock)
)
thread2 = threading.Thread(
	target=generate_numbers,
	args=('second', lock)
)
thread1.start()
thread2.start()
