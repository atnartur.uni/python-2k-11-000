import multiprocessing
import random


def generate_numbers(thread_name, queue):
	for i in range(10):
		number = random.randrange(0, 100)
		print(thread_name, i, number)
		queue.put(number)


if __name__ == '__main__':
	queue = multiprocessing.Queue()

	process1 = multiprocessing.Process(
		target=generate_numbers, 
		args=('first', queue)
	)
	process2 = multiprocessing.Process(
		target=generate_numbers, 
		args=('second', queue)
	)

	process1.start()
	process2.start()
	process1.join()
	process2.join()

	while not queue.empty():
		print(queue.get())
