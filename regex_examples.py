import re

regex = r"([0-9]{2})\.([0-9]{2})\.([0-9]{4})"
pattern = re.compile(regex)
print(pattern.match('28.10.2021'))
print(pattern.match('Сегодня 28.10.2021 года'))
print(pattern.match('eto ne data'))
print(pattern.match('здравствуйте'))

print(pattern.findall('Сегодня 28.10.2021 года 30.10.2021'))

# https://regex101.com/
test_str = ("asdfdsaf 28.10.2021 asdfadsfasdf\n"
	"asdfdsaf 28.10.2021\n"
	"28.10.2021 asdfadsfasdf\n"
	"28.10.3021\n"
	"42.10.2021\n"
	"32.10.2021\n"
	"12.15.2021\n"
	"12.25.2021\n")

matches = re.finditer(regex, test_str, re.MULTILINE)

for matchNum, match in enumerate(matches, start=1):
    
    print ("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum = matchNum, start = match.start(), end = match.end(), match = match.group()))
    
    for groupNum in range(0, len(match.groups())):
        groupNum = groupNum + 1
        
        print ("Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))
