import random
from abc import ABC, abstractmethod


class Animal(ABC):
    count = 0

    def __init__(self, name, weight, color):
        self.name = name
        self.weight = weight
        self.color = color
        self.fullness = 0
        print(self.count)
        Animal.count += 1

    def __str__(self):
        return f'<Animal {self.name}>'

    def eat(self, fullness=1):
        self.fullness += fullness

    def say(self, word):
        print(f'{self.name}: {word}')

    @abstractmethod
    def sleep(self):
        pass

    @staticmethod
    def static_method_example():
        print('123')

    @classmethod
    def class_method_example(cls):
        print('animal instance count', cls.count)


Animal.static_method_example()
Animal.class_method_example()


class LoudMeow():
    def mur(self):
        print('MUR!')

    def meow(self):
        print('MEOW! ' * 3)


class Cat(Animal):
    def __str__(self):
        return f'<Cat {self.name}>'

    def eat(self, fullness=1):
        want_to_eat = random.random() > 0.5
        if want_to_eat:
            super(Cat, self).eat(fullness)

    def meow(self):
        want_to_meow = random.random() > 0.5
        if want_to_meow:
            self.__meow()

    def __meow(self):
        self.say('MEOW!')

    def sleep(self):
        print('sleeeep')


class LoudCat(LoudMeow, Cat):
    pass


cat = LoudCat("Шока", 5, "black")
cat.mur()
print(cat)
print(cat.name, cat.color)
cat.eat()
cat.eat()
cat.eat(5)
print('fullness:', cat.fullness)
cat.meow()
cat.meow()
cat.meow()
cat.meow()
print(dir(cat))

cat2 = Cat("Барсик", 6, "white")
print(cat2)

print(Cat.count)

print('-------')


class Number():
    def __init__(self, number):
        self.number = number

    def __str__(self):
        return str(self.number)

    def __add__(self, other):
        number1 = self
        number2 = other
        return Number(number1.number + number2.number)


number = Number(5)
print(number)
number2 = Number(6)

result = number + number2
print(result)
