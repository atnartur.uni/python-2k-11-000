from pprint import pprint


def input_number(prompt):
	number = input(prompt)
	number = number.replace(',', '.')
	try:
		return float(number)
	except ValueError:
		print('Вы ввели неправильное число')
		return None


def add(number, number2):
	return number + number2


def sub(number, number2):
	return number - number2


def mult(number, number2):
	return number * number2


def div(number, number2):
	try:
		return number / number2
	except ZeroDivisionError:
		print('На ноль делить нельзя')
		exit()

operator_functions = {
	'+': add,
	'-': sub,
	'*': mult,
	'/': div
}

operators = operator_functions.keys()
operators_for_print = ', '.join(operators)


def calculate():
	number = input_number('Введите первое число: ')
	if number is None:
		return
	
	operator = input(f'Введите операцию ({operators_for_print}): ')
	try:
		func = operator_functions[operator]
	except KeyError:
		print('Вы ввели неправильный оператор')
		return

	number2 = input_number('Введите второе число: ')
	if number2 is None:
		return
	
	result = func(number, number2)

	result_for_print = '{number:.2f} {operator} {number2:.2f} = {result:.2f}'.format(
		number=number,
		operator=operator,
		number2=number2,
		result=result,
	)

	print(result_for_print)

	return {
		'number': number,
		'number2': number2,
		'operator': operator,
		'result': result
	}

history = []

while True:
	result = calculate()
	print('-' * 10)
	if result is None:
		continue
	history.append(result)
	pprint(history)