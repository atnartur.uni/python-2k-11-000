import requests

# pip install beatufoulsoup4

import bs4

response = requests.get(
    'https://habr.com',
    headers={
        'Accept-Language': 'ru;q=0.8,en-US;q=0.5,en;q=0.3'
    }
)
html = response.text

tree = bs4.BeautifulSoup(html, 'html.parser')
promo_link = tree.select_one(
    '.tm-header__become-author-btn'
)
print(promo_link.attrs['href'])

articles = tree.select('article.tm-articles-list__item')
for article in articles:
    link = article.select_one('.tm-article-snippet__title-link')
    if link is None:
        continue
    print('-', link.text, link.attrs['href'])

