import pytest


def div(x, y):
    return x / y


def zero_div_handler_decorator(func):
    def wrapper(*args, **kwargs):
        print('DIV!')
        x, y = args
        if y == 0:
            return None
        return func(*args, **kwargs)

    return wrapper


decorated_div = zero_div_handler_decorator(div)

print(div(6, 3))
print(decorated_div(6, 0))


@zero_div_handler_decorator
def decorated_div2(x, y):
    return x / y


print(decorated_div(5, 0))
print(decorated_div(5, 2))


@pytest.mark.parametrize(
    "x, y, expected_result",
    [
        (6, 3, 2),
        (5, 5, 1),
        (10, 2, 5)
    ]
)
def test_div(x, y, expected_result):
    result = div(x, y)
    assert result == expected_result


def test_decorated_div():
    result = decorated_div(6, 0)
    assert result is None


def test_div_with_exception():
    with pytest.raises(ZeroDivisionError):
        decorated_div(1, 0)
