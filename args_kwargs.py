rus_eng = {'Кошка': 'Cat', 'Собака': 'Dog'}
rus_tat = {'Мама': 'Әни'}
full_dict = dict(**rus_eng, **rus_tat)


def sum(*args, **kwargs):
	only_more_than_0 = kwargs.get('only_more_than_0', False)
	result_sum = 0
	for number in args:
		if only_more_than_0 and number < 0:
			continue
		result_sum += number
	return result_sum


numbers = [int(s) for s in input().strip().split(' ')]
print(numbers)
print(sum(*numbers))