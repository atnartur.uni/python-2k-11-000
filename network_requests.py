from unittest import mock

import requests


# response = requests.get(
#     'http://77.88.55.88',
#     headers={
#         'Host': 'yandex.ua'
#     }
# )
# with open('yandex.html', 'w') as file:
#     file.write(response.text)
# print(response.status_code)
# print(response.text[:20])
# print('Казань', 'Казань' in response.text)
# print('Киев', 'Киев' in response.text)

# response = requests.post(
#     'http://postman-echo.com/post',
#     data={
#         'a': 1,
#         'b': 2
#     }
# )
# print(response.headers['Content-Type'])
# pprint(response.json())
#
# with open('cat.jpg', 'wb') as file:
#     response = requests.get('https://http.cat/404')
#     print(response.headers['Content-Type'])
#     file.write(response.content)


def get_facts():
    response = requests.get('https://dog-facts-api.herokuapp.com/api/v1/resources/dogs/all')
    print('REQUEST!')
    return response.json()


def get_facts_count():
    facts = get_facts()
    return len(facts)


def test_facts_count():
    with mock.patch(
        'network_requests.get_facts',
        side_effect=lambda: [{'fact': 1}, {'fact': 2}]
    ):
        actual = get_facts_count()
    assert actual != 0


if __name__ == '__main__':
    print(get_facts_count())
